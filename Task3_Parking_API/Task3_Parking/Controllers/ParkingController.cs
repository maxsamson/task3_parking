﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Task3_Parking.Classes;
using Task3_Parking.Interfaces;

namespace Task3_Parking.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ParkingController : Controller
    {
        private readonly IParkingService parkingService;

        public ParkingController(IParkingService parkingService)
        {
            this.parkingService = parkingService;
        }

        [HttpGet]
        [Route("cars")]
        public JsonResult GetCars()
        {
            return Json(parkingService.GetCars());
        }

        [HttpGet()]
        [Route("cars/{id}")]
        public JsonResult GetCarById(int id)
        {
            return Json(parkingService.GetCarById(id));
        }

        [HttpGet]
        [Route("freeplaces")]
        public JsonResult GetFreePlaces()
        {
            return Json(parkingService.GetFreePlaces());
        }

        [HttpGet]
        [Route("log/whole")]
        public JsonResult GetLogWhole()
        {
            return Json(parkingService.GetLog());
        }

        [HttpGet]
        [Route("log/oneminute")]
        public JsonResult GetLogForOneMinute()
        {
            return Json(parkingService.GetHistoryForOneMinute());
        }

        [HttpGet]
        [Route("income/whole")]
        public JsonResult GetIncomeWhole()
        {
            return Json(parkingService.GetCommonIncome());
        }

        [HttpGet]
        [Route("income/oneminute")]
        public JsonResult GetIncomeForOneMinute()
        {
            return Json(parkingService.GetIncomeForOneMinute());
        }

        // POST api/parking/cars
        [HttpPost]
        [Route("cars")]
        public JsonResult Post([FromBody] dynamic json)
        {
            var car = Newtonsoft.Json.JsonConvert.DeserializeObject<Car>(json);
            return Json(parkingService.CreateCar((double)car.Balance, (CarType)car.Type));
        }

        // POST api/parking/cars/balance
        [HttpPost]
        [Route("cars/balance")]
        public JsonResult Patch([FromBody] dynamic json)
        {
            var car = Newtonsoft.Json.JsonConvert.DeserializeObject<Car>(json);
            return Json(parkingService.UpdateCarBalance((int)car.Id, (double)car.Balance));
        }

        // DELETE api/parking/cars
        [HttpDelete]
        [Route("cars/{id}")]
        public JsonResult Delete(string id)
        {
            return Json(parkingService.DeleteCarById(Convert.ToInt32(id)));
        }
    }
}
