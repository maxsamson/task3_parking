﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Task3_Parking.Classes;

namespace Task3_Parking.Interfaces
{
    public interface IParkingService
    {
        List<Car> GetCars();

        Car GetCarById(int id);

        int GetFreePlaces();

        List<string> GetLog();

        List<string> GetHistoryForOneMinute();

        double GetCommonIncome();

        double GetIncomeForOneMinute();

        int CreateCar(double balance, CarType type);

        double UpdateCarBalance(int id, double money);

        int DeleteCarById(int id);
    }
}
