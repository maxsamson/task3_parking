﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Task3_Parking.Classes;
using Task3_Parking.Interfaces;

namespace Task3_Parking.Services
{
    public class ParkingService : IParkingService
    {
        private readonly Parking parking;

        public ParkingService(Parking parking)
        {
            this.parking = parking;
        }

        public List<Car> GetCars()
        {
            return parking.ListOfCars;
        }

        public Car GetCarById(int id)
        {
            return parking.GetCar(id);
        }

        public int GetFreePlaces()
        {
            return parking.ShowFreePlaces();
        }

        public List<string> GetLog()
        {
            return parking.GetLog();

        }

        public List<string> GetHistoryForOneMinute()
        {
            var time = DateTime.Now;
            time = time.AddMinutes(-1);
            List<string> list = new List<string>();
            var items = parking.ListOfTransactions.Where(x => x.Date >= time);
            foreach (var item in items)
            {
                list.Add(item.ToString());
            }
            return list;

        }

        public double GetCommonIncome()
        {
            return parking.Income;
        }

        public double GetIncomeForOneMinute()
        {
            var time = DateTime.Now;
            time = time.AddMinutes(-1);
            double sum = 0;
            var items = parking.ListOfTransactions.Where(x => x.Date >= time);
            foreach (var item in items)
            {
                sum += item.Tax;
            }
            return sum;
        }

        public int CreateCar(double balance, CarType type)
        {
            return parking.AddCar(balance, type);
        }

        public double UpdateCarBalance(int id, double money)
        {
            return parking.TopUpBalance(id, money);
        }

        public int DeleteCarById(int id)
        {
            return parking.RemoveCar(id);
        }
    }
}
