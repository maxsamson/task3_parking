﻿using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;

namespace Task3_Parking.Classes
{
    public class Parking
    {
        public string TransactionFileName { get; } = Directory.GetParent(Directory.GetParent(Directory.GetCurrentDirectory()).FullName).FullName + "\\AppData\\Transactions.log";

        private readonly Settings _settings;

        public List<Car> ListOfCars { get; }
        public List<Transaction> ListOfTransactions { get; }

        private readonly TimerCallback _tcbTrans;
        private readonly Dictionary<Car, Timer> _dictTimers;

        public int FreePlaces { get; private set; }
        public double Income { get; private set; }

        public Parking(IOptions<Settings> ownerOptions)
        {
            _settings = ownerOptions.Value;
            ListOfCars = new List<Car>();
            ListOfTransactions = new List<Transaction>();
            _tcbTrans = new TimerCallback(Transaction);
            _dictTimers = new Dictionary<Car, Timer>();
            var tcbSunTrans = new TimerCallback(SumOfTransactions);
            var timer = new Timer(tcbSunTrans, null, 0, 5000);
            FreePlaces = _settings.ParkingSpace;
            Income = 0;
        }

        ~Parking()
        {
            foreach (var item in _dictTimers)
            {
                item.Value.Dispose();
            }
        }

        public int AddCar(double balance, CarType type)
        {
            if (FreePlaces == 0)
            {
                return -1;
            }
            int carId;
            Car car;
            try
            {
                carId = ListOfCars[ListOfCars.Count - 1].Id;
                car = new Car(++carId, balance, type);
                ListOfCars.Add(car);
            }
            catch(ArgumentOutOfRangeException)
            {
                carId = 1;
                car = new Car(carId, balance, type);
                ListOfCars.Add(car);
            }
            FreePlaces--;

            _dictTimers.Add(ListOfCars[ListOfCars.Count - 1], new Timer(_tcbTrans, carId, _settings.TimeOut * 1000, _settings.TimeOut * 1000));

            return car.Id;
        }

        public int RemoveCar(int id)
        {
            try
            {
                var car = ListOfCars.Find(obj => obj.Id == id);
                _dictTimers[car].Dispose();
                _dictTimers.Remove(car);
                ListOfCars.Remove(car);
            }
            catch
            {
                return -1;
            }
            FreePlaces++;
            return 1;
        }

        public Car GetCar(int id)
        {
            try
            {
                var car = ListOfCars.Find(obj => obj.Id == id);
                return car;
            }
            catch
            {
                return null;
            }
        }

        public double TopUpBalance(int carId, double money)
        {
            try
            {
                int index = ListOfCars.FindIndex(car => car.Id == carId);

                if (ListOfCars[index].Fine == 0)
                {
                    ListOfCars[index].Balance += money;
                }
                else
                {
                    if (money <= ListOfCars[index].Fine)
                    {
                        ListOfCars[index].Fine -= money;
                        Income += money;
                        int transId;
                        try
                        {
                            transId = ListOfTransactions[ListOfTransactions.Count - 1].IdTrans;
                            ListOfTransactions.Add(new Transaction(transId++, carId, money));
                        }
                        catch (ArgumentOutOfRangeException)
                        {
                            transId = 1;
                            ListOfTransactions.Add(new Transaction(transId++, carId, money));
                        }
                    }
                    else
                    {
                        Income += ListOfCars[index].Fine;
                        int transId;
                        try
                        {
                            transId = ListOfTransactions[ListOfTransactions.Count - 1].IdTrans;
                            ListOfTransactions.Add(new Transaction(transId++, carId, ListOfCars[index].Fine));
                        }
                        catch (ArgumentOutOfRangeException)
                        {
                            transId = 1;
                            ListOfTransactions.Add(new Transaction(transId++, carId, ListOfCars[index].Fine));
                        }
                        ListOfCars[index].Balance += money - ListOfCars[index].Fine;
                        ListOfCars[index].Fine = 0;
                    }
                }
                return ListOfCars[index].Balance;
            }
            catch (FormatException)
            {
                return -1;
            }
            catch (ArgumentOutOfRangeException)
            {
                return -1;
            }
        }

        public int ShowFreePlaces()
        {
            return FreePlaces;
        }

        private void Transaction(object obj)
        {
            try
            {
                foreach(var car in ListOfCars)
                {
                    double priceOfTrans = _settings.Prices[Enum.GetName(typeof(CarType), car.Type)];
                    if (priceOfTrans > car.Balance)
                    {
                        priceOfTrans *= _settings.Fine;
                        car.Fine += priceOfTrans;
                    }
                    else
                    {
                        car.Balance -= priceOfTrans;
                        Income += priceOfTrans;
                        int transId;
                        try
                        {
                            transId = ListOfTransactions[ListOfTransactions.Count - 1].IdTrans;
                            ListOfTransactions.Add(new Transaction(transId++, car.Id, priceOfTrans));
                        }
                        catch (ArgumentOutOfRangeException)
                        {
                            transId = 1;
                            ListOfTransactions.Add(new Transaction(transId++, car.Id, priceOfTrans));
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.WriteException(ex.Message, "Parking.log");
            }
        }

        private void SumOfTransactions(object obj)
        {
            link:
            double sum = 0;
            var time = DateTime.Now;
            try
            {
                time = time.AddMinutes(-1);
                foreach (var item in ListOfTransactions)
                {
                    if (item.Date >= time)
                    {
                        sum += item.Tax;
                    }
                    else
                    {
                        ListOfTransactions.Remove(item);
                    }
                }
            }
            catch
            {
                goto link;
            }

            using (var sw = new StreamWriter(TransactionFileName, true))
            {
                sw.WriteLine("Sum: " + sum + ", time: " + time);
            }
        }

        public List<string> GetLog()
        {
            var list = new List<string>();
            if (File.Exists(TransactionFileName))
            {
                using (var sr = new StreamReader(TransactionFileName))
                {
                    string s;
                    while ((s = sr.ReadLine()) != null)
                    {
                        list.Add(s);
                    }
                }
            }
            return list;
        }
    }
}
