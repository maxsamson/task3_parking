﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task3_Parking.Classes
{
    public class Transaction
    {
        private readonly object _locker = new object();
        public DateTime Date { get; set; }
        public int IdTrans { get; set; }
        public int IdCar { get; set; }
        public double Tax { get; set; }

        public Transaction(int id, int carId, double tax)
        {
            lock (_locker)
            {
                Date = DateTime.Now;
                IdTrans = id;
                IdCar = carId;
                Tax = tax;
            }
        }

        public override string ToString()
        {
            return "Transaction id: " + IdTrans + "\nCar id: " + IdCar + "\nSum of transaction: " + Tax + "\nDate: " + Date + "\n";
        }
    }
}
