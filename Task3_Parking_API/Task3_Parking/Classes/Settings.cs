﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task3_Parking.Classes
{
    public class Settings
    {
        public Dictionary<string, double> Prices { get; set; }
        public int TimeOut { get; set; }
        public int ParkingSpace { get; set; }
        public double Fine { get; set; }
    }
}
