﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task3_Parking.Classes
{
    public enum CarType { Truck, Passenger, Bus, Motorcycle };

    public class Car
    {
        private readonly object _locker = new object();
        public int Id { get; set; }
        public double Balance { get; set; }
        public double Fine { get; set; }
        public CarType Type { get; set; }

        public Car(int id, double balance, CarType type)
        {
            Id = id;
            Balance = balance;
            Fine = 0;
            Type = type;
        }
    }
}
