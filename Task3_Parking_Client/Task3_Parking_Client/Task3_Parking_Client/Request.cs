﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;

namespace Task3_Parking_Client
{
    public static class Request
    {
        private static JsonSerializerSettings settings = new JsonSerializerSettings();
        private static readonly Uri _url = new Uri("http://localhost:10436/api/parking/");

        static Request()
        {
            settings.DateFormatString = "YYYY-MM-DDTHH:mm:ss.FFFZ";
        }

        public static T SendData<T>(object data, string uri)
        {
            using (HttpClient client = new HttpClient())
            {
                var response = client.PostAsJsonAsync(_url + uri, JsonConvert.SerializeObject(data)).Result;

                return JsonConvert.DeserializeObject<T>(response.Content.ReadAsStringAsync().Result, settings);
            }
        }

        public static List<T> GetDataList<T>(string uri)
        {
            using (HttpClient client = new HttpClient())
            {
                var result = client.GetStringAsync(_url + uri).Result;

                return JsonConvert.DeserializeObject<List<T>>(result, settings);
            }
        }

        public static T GetData<T>(string uri)
        {
            using (HttpClient client = new HttpClient())
            {
                var result = client.GetStringAsync(_url + uri).Result;

                return JsonConvert.DeserializeObject<T>(result, settings);
            }
        }

        public static T GetData<T>(string id, string uri)
        {
            using (HttpClient client = new HttpClient())
            {
                var result = client.GetStringAsync(_url + uri + "/" + id).Result;

                return JsonConvert.DeserializeObject<T>(result, settings);
            }
        }

        public static T DeleteData<T>(string id, string uri)
        {
            using (HttpClient client = new HttpClient())
            {
                var response = client.DeleteAsync(_url + uri + "/" + id).Result;
                return JsonConvert.DeserializeObject<T>(response.Content.ReadAsStringAsync().Result, settings);
            }
        }
    }
}
