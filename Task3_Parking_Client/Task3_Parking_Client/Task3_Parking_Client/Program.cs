﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Task3_Parking_Client
{
    public class Program
    {
        public static List<Menu> ListOfMenus = new List<Menu>();

        private static void Main(string[] args)
        {
            ListOfMenus.Add(new Menu(StartParking, Exit));
            ListOfMenus.Add(new Menu(AddCar, RemoveCar, ShowCar, ShowCars, TopUpBalance, ShowFreePlaces, ShowLog, ShowHistoryForOneMinute, ShowCommonIncome, ShowIncomeForOneMinute, Exit));
            ListOfMenus.Add(new Menu(PickTruck, PickPassenger, PickBus, PickMotorcycle));
            ListOfMenus.Add(new Menu(TurnBack));
            ListOfMenus[0].Show(false);
        }

        private static void StartParking()
        {
            Console.Clear();
            ListOfMenus[1].Show(false);
        }

        private static void Exit()
        {
            Environment.Exit(0);
        }

        private static void AddCar()
        {
            Console.Clear();
            Console.WriteLine("Choose a type of your car: ");
            ListOfMenus[2].Show(false);
        }

        private static void PickTruck()
        {
            Console.Clear();
            Console.WriteLine("Write a balance of your car: ");
            try
            {
                var bal = Convert.ToDouble(Console.ReadLine());
                var result = Request.SendData<int>(new { balance = bal, type = CarType.Truck }, "cars");
                if (result == -1)
                    Console.WriteLine("There is not free parking space.");
                else
                    Console.WriteLine($"Your car has been successfully added. It has id = {result}");
            }
            catch
            {
                Console.WriteLine("You haven't been wrote a balance, try again.");
            }
            ListOfMenus[3].Show(false);
        }

        private static void PickPassenger()
        {
            Console.Clear();
            Console.WriteLine("Write a balance of your car: ");
            var bal = Convert.ToDouble(Console.ReadLine());
            var result = Request.SendData<int>(new { balance = bal, type = CarType.Passenger }, "cars");
            if (result == -1)
                Console.WriteLine("There is not free parking space.");
            else
                Console.WriteLine($"Your car has been successfully added. It has id = {result}");
            ListOfMenus[3].Show(false);
        }

        private static void PickBus()
        {
            Console.Clear();
            Console.WriteLine("Write a balance of your car: ");
            var bal = Convert.ToDouble(Console.ReadLine());
            var result = Request.SendData<int>(new { balance = bal, type = CarType.Bus }, "cars");
            if (result == -1)
                Console.WriteLine("There is not free parking space.");
            else
                Console.WriteLine($"Your car has been successfully added. It has id = {result}");
            ListOfMenus[3].Show(false);
        }

        private static void PickMotorcycle()
        {
            Console.Clear();
            Console.WriteLine("Write a balance of your car: ");
            var bal = Convert.ToDouble(Console.ReadLine());
            var result = Request.SendData<int>(new { balance = bal, type = CarType.Motorcycle }, "cars");
            if (result == -1)
                Console.WriteLine("There is not free parking space.");
            else
                Console.WriteLine($"Your car has been successfully added. It has id = {result}");
            ListOfMenus[3].Show(false);
        }

        private static void RemoveCar()
        {
            Console.Clear();
            Console.WriteLine("Write id of your car: ");
            var id = Convert.ToInt32(Console.ReadLine());
            if (Request.DeleteData<int>(id.ToString(), "cars") == 1)
                Console.WriteLine("Your car has been successfully removed.");
            else
                Console.WriteLine("Didn't find any car with current id. Check id you wrote and try again.");
            ListOfMenus[3].Show(false);
        }

        private static void ShowCar()
        {
            Console.Clear();
            Console.WriteLine("Write id of your car: ");
            var id = Convert.ToInt32(Console.ReadLine());
            var car = Request.GetData<Car>(id.ToString(), "cars");
            Console.Clear();
            if (car != null)
                car.ShowCar();
            else
                Console.WriteLine("Didn't find any car with current id. Check id you wrote and try again.");
            Console.WriteLine();
            ListOfMenus[3].Show(false);
        }

        private static void ShowCars()
        {
            Console.Clear();
            Console.Clear();
            foreach(var car in Request.GetDataList<Car>("cars"))
            {
                car.ShowCar();
                Console.WriteLine();
            }
            Console.WriteLine();
            ListOfMenus[3].Show(false);
        }

        private static void TopUpBalance()
        {
            Console.Clear();
            try
            {
                Console.WriteLine("Write id of your car: ");
                int id = Convert.ToInt32(Console.ReadLine());
                Console.WriteLine("Write money to top up the balance: ");
                double money = Convert.ToDouble(Console.ReadLine());
                double balance = Request.SendData<double>(new { Id = id, balance = money }, "cars/balance");
                if (balance != -1)
                    Console.WriteLine($"Balance of your car was changed successfully! It equals {balance}.");
                else
                    Console.WriteLine("Didn't find any car with current id. Check id you wrote and try again.");
            }
            catch
            {
                Console.WriteLine("You haven't been wrote an id or money to top up the balance, try again.");
            }
            ListOfMenus[3].Show(false);
        }

        private static void ShowFreePlaces()
        {
            Console.Clear();
            Console.WriteLine($"Parking places: {Request.GetData<int>("freeplaces")}/10");
            ListOfMenus[3].Show(false);
        }

        private static void ShowLog()
        {
            Console.Clear();
            List<string> listLog = Request.GetDataList<string>("log/whole");
            foreach (var item in listLog)
            {
                Console.WriteLine(item);
            }
            ListOfMenus[3].Show(false);
        }

        private static void ShowHistoryForOneMinute()
        {
            Console.Clear();
            List<string> listLog = Request.GetDataList<string>("log/oneminute");
            foreach (var item in listLog)
            {
                Console.WriteLine(item);
            }
            ListOfMenus[3].Show(false);
        }

        private static void ShowCommonIncome()
        {
            Console.Clear();
            Console.WriteLine("Common Income: " + Request.GetData<double>("income/whole"));
            ListOfMenus[3].Show(false);
        }

        private static void ShowIncomeForOneMinute()
        {
            Console.Clear();
            Console.WriteLine("Income for last minute: " + Request.GetData<double>("income/oneminute"));
            ListOfMenus[3].Show(false);
        }

        private static void TurnBack()
        {
            Console.Clear();
            ListOfMenus[1].Show(false);
        }
    }
}
